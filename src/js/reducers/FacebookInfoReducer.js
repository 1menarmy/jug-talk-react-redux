/**
 * Created by kanitmar on 06.02.17.
 */
import axios from 'axios';
const initialState = {
    fetching: false,
    fetched: false,
    users: [],
    error: null,
};

export const facebookInfoReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH_INFO_START': {
            return {...state, fetching: true};
            break;
        }
        case 'FETCH_INFO_SUCCESS': {
            return {...state, fetched: true, fetching:false, users: action.payload};
            break;
        }
        case 'FETCH_INFO_FAILURE':
            return {...state, error: action.payload}
            break;
        default:
            return state;
    }
}

export const fetchFacebookInfo = () => {
    return (dispatch) => {
        dispatch({type: 'FETCH_INFO_START'});
        axios.get('https://jsonplaceholder.typicode.com/users')
            .then((response) => {
                setTimeout(()=>{dispatch({type: 'FETCH_INFO_SUCCESS', payload: response.data})},2000);
            })
            .catch((error)=> {dispatch({type: 'FETCH_INFO_FAILURE', payload: error})
            })
    };
}