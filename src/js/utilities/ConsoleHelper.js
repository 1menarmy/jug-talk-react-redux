/**
 * Created by kanitmar on 04.02.17.
 */
export const doConsoleLogForLifecycleMethods = (when) => {console.log("This is called before "+when+" method was called");}
export const doConsoleLog = (wholeText) => {console.log(wholeText);}