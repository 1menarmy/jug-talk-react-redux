/**
 * Created by kanitmar on 04.02.17.
 */
export const firstSampleParagraphText = "as we already mentioned above, the purpose of the &ltheader&gt and elements is to wrap header and" +
    "footer content, respectively. In our particular example the element contains a logo image, and" +
    "the element contains a copyright notice, but you could add more elaborate content if you wished." +
    "Also note that you can have more than one header and footer on each page - as well as the top" +
    "level header and footer we have just discussed, you could also have a and element nested inside" +
    "each, in which case they would just apply to that particular article. Adding to our above example";
export const secondSampleParagraphText = "Bring Me the Horizon, often known by the acronym BMTH, are a British rock band from Sheffield, " +
    "South Yorkshire. Formed in 2004, the group currently consists of vocalist Oliver Sykes, " +
    "guitarist Lee Malia, bassist Matt Kean, drummer Matt Nicholls and keyboardist Jordan Fish." +
    " They are signed to RCA Records globally and Columbia Records exclusively in the United States. " +
    "The style of their early work, including their debut album Count Your Blessings, " +
    "has primarily been described as deathcore, but they started to adopt a more eclectic style of metalcore on subsequent albums. " +
    "Furthermore, their latest album That's the Spirit marked a shift in their sound to less aggressive rock music styles.";
export const thirdSampleParagraphText = "Architects are a British metalcore band from Brighton, England. " +
    "The band currently consists of vocalist Sam Carter, drummer Dan Searle, bassist Alex Dean and guitarist Adam Christianson. " +
    "The band's first name was Inharmonic, which was swiftly changed to Counting the Days," +
    " and finally to Architects after a couple of years. " +
    "They have released seven studio albums and one split EP with Dead Swans to date.";