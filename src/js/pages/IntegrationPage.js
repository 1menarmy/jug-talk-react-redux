/**
 * Created by kanitmar on 04.02.17.
 */
import React from 'react';
import Header from './../components/Header';
import Main from './../components/Main';
import Footer from './../components/Footer';
import Navigation from './../components/Navigation';

export default class TestingPage extends React.Component {

    constructor() {
        super();
    }

    render() {
        const homePageHeaderTitle = 'Integration page SMALLER header Title';
        const homePageBiggerHeaderTitle = 'Integration page B.I.G header Title';
        return (
            <div>
                <Navigation/>
                <Header
                    homePageHeaderTitle={homePageHeaderTitle}
                    homePageBiggerHeaderTitle={homePageBiggerHeaderTitle}
                />
                <Main />
                <Main />
                <Footer someNonUsedAttr="not used"/>
            </div>
        );
    }
}