/**
 * Created by kanitmar on 04.02.17.
 */
import React from 'react';
import Header from './../components/Header';
import Main from './../components/Main';
import Footer from './../components/Footer';
import Paragraph from './../components/Paragraph';
import Navigation from './../components/Navigation';
import {thirdSampleParagraphText} from './../utilities/Paragraphs';

export default class HomePage extends React.Component {

    constructor() {
        super();
        this.state = {
            paragraph: thirdSampleParagraphText,
            customParagraph: ''
        }
    }

    addParagraph(text) {
        this.setState({customParagraph: text});
    }

    render() {
        const homePageHeaderTitle = 'Home page SMALLER header Title';
        const homePageBiggerHeaderTitle = 'Home page B.I.G header Title';
        return (
            <div>
                <Navigation/>
                <Header
                    homePageHeaderTitle={homePageHeaderTitle}
                    homePageBiggerHeaderTitle={homePageBiggerHeaderTitle}
                />
                <Main addParagraph={this.addParagraph.bind(this)}>
                    <Paragraph text={this.state.customParagraph}/>
                </Main>
                <Footer someNonUsedAttr="not used"/>
            </div>
        );
    }
}