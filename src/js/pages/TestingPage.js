/**
 * Created by kanitmar on 04.02.17.
 */
import React from 'react';
import Header from './../components/Header';
import Main from './../components/Main';
import Footer from './../components/Footer';
import Navigation from './../components/Navigation';
import FacebookInfo from './../components/FacebookInfo';

export default class TestingPage extends React.Component {

    constructor() {
        super();
    }

    render() {
        const homePageHeaderTitle = 'Testing page SMALLER header Title';
        const homePageBiggerHeaderTitle = 'Testing page B.I.G header Title';
        return (
            <div>
                <FacebookInfo/>
                <Navigation/>
                <Header
                    homePageHeaderTitle={homePageHeaderTitle}
                    homePageBiggerHeaderTitle={homePageBiggerHeaderTitle}
                />
                <Main />
                <Footer someNonUsedAttr="not used"/>
            </div>
        );
    }
}