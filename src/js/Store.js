/**
 * Created by kanitmar on 06.02.17.
 */
import {createStore, applyMiddleware, combineReducers} from 'redux';
import {counterReducer} from './reducers/CounterReducer';
import {todosReducer} from './reducers/TodosReducer';
import {facebookInfoReducer} from './reducers/FacebookInfoReducer';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
    counterReducer,
    todosReducer,
    facebookInfoReducer,
});
const middleware = applyMiddleware(thunk, createLogger());
export default createStore(rootReducer, middleware);