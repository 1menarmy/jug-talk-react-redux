/**
 * Created by kanitmar on 04.02.17.
 */
import React from 'react';

export default class Footer extends React.Component{
    render(){
        return (
            <footer>
                <h3>Some little bigger footer text</h3>
            </footer>
        );
    }
}