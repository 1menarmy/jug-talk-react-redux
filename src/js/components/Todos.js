/**
 * Created by kanitmar on 06.02.17.
 */
import React from 'react';

export default class Todos extends React.Component {

    handleAddTodo(event) {
        event.preventDefault();
        let inputValue = event.target.elements["todoInput"].value.trim();
        if (inputValue && this.props.addTodo) {
            this.props.addTodo(inputValue);
            event.target.elements["todoInput"].value='';
        }
    }

    render() {
        const {todos} = this.props;
        return (
            <div>
                <form onSubmit={this.handleAddTodo.bind(this)}>
                    <input name="todoInput"/>
                    <button type="submit">Add TODO!</button>
                </form>
                <div>
                    TABLE OF TODOS <br/>
                    ==============
                    <ul>
                        {todos.map((todo, index) =>
                            <li key={index}>{todo}</li>
                        )}
                    </ul>
                </div>
            </div>
        );
    }
}