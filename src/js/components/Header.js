/**
 * Created by kanitmar on 04.02.17.
 */
import React from 'react';
import {doConsoleLogForLifecycleMethods, doConsoleLog} from '../utilities/ConsoleHelper';
import Title from './Title';

export default class Header extends React.Component {
    constructor(props) {
        super();
        doConsoleLog("'Header' component created!");
        this.state = {
            h3text: props.homePageHeaderTitle,
        }
    }

    componentDidMount() {
        doConsoleLogForLifecycleMethods('before');
        setTimeout(()=> {
            this.setState(
                {
                    h3text: 'U.P.D.A.T.E.D header text for the app!!!'
                }
            )
        }, 3000);
    }

    componentWillMount() {
        doConsoleLogForLifecycleMethods('after');
    }

    render() {
        doConsoleLog("I am rendering 'Header component'");
        const {h3text} = this.state;
        const componentArray = <div>
            <Title proppedTitleText={this.props.homePageBiggerHeaderTitle}/>
            <h3>{h3text + ' with some suffix text' + (()=> {
                return 'and computed text';
            })()}</h3>
        </div>
        return (
            <header>
                {componentArray}
            </header>
        );
    }
}