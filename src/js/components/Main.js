/**
 * Created by kanitmar on 04.02.17.
 */
import React from 'react';
import {firstSampleParagraphText, secondSampleParagraphText} from './../utilities/Paragraphs';
export default class Main extends React.Component {
    constructor() {
        super();
        this.state = {
            paragraphText: firstSampleParagraphText
        }
    }

    componentDidMount() {
        setTimeout(()=> {
            this.setState({paragraphText: secondSampleParagraphText});
        }, 2000);
    }

    handleChange(e) {
        if (this.props.addParagraph) {
            this.props.addParagraph(e.target.value);
        } else{
            console.log("I am silly instance of Main component i cannot addParagraphs");
        }
    }

    render() {
        return (
            <section>
                <article>
                    Add your own paragraph here: <input type="text" onChange={this.handleChange.bind(this)}/>
                    <p>
                        {this.state.paragraphText}
                    </p>
                    {this.props.children}
                </article>
            </section>
        );
    }
}