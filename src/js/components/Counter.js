/**
 * Created by kanitmar on 05.02.17.
 */
import React from 'react';

export default class Counter extends React.Component {

    incrementIfOdd(){
        if (this.props.value % 2 !== 0) {
            this.props.onIncrement();
        }
    }

    incrementAsync() {
        setTimeout(this.props.onIncrement, 1000);
    }

    render() {
        console.log("This props in counter render method", this.props);
        const { value, onIncrement, onDecrement } = this.props
        return (
            <p>
                Clicked: {value} times
                {' '}
                <button onClick={onIncrement}>
                    +
                </button>
                {' '}
                <button onClick={onDecrement}>
                    -
                </button>
                {' '}
                <button onClick={this.incrementIfOdd.bind(this)}>
                    Increment if odd
                </button>
                {' '}
                <button onClick={this.incrementAsync.bind(this)}>
                    Increment async
                </button>
            </p>
        )
    }
}