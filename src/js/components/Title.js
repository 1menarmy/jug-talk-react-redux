/**
 * Created by kanitmar on 04.02.17.
 */
import React from 'react';

export default class Title extends React.Component {
    render() {
        return (
            <h1>{this.props.proppedTitleText}</h1>
        );
    }
}