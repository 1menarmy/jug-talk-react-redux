/**
 * Created by kanitmar on 05.02.17.
 */
import React from 'react';
export default class Paragraph extends React.Component {
    render() {
        return (
            <article>
                <p>{this.props.text}</p>
            </article>
        );
    }
}