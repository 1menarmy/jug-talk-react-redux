/**
 * Created by kanitmar on 06.02.17.
 */
import React from 'react';
import {connect} from "react-redux";
import {fetchFacebookInfo} from './../reducers/FacebookInfoReducer';

@connect((store)=> {
    return {
        users: store.facebookInfoReducer.users,
        fetching:store.facebookInfoReducer.fetching
    }
})
export default class FacebookInfo extends React.Component {

    handleClick() {
        this.props.dispatch(fetchFacebookInfo());
    }

    render() {
        console.log("This.props in facebook info", this.props);
        return (
            <div>
                <input type="button" value="Load BIGGEST Facebook users!" onClick={this.handleClick.bind(this)}/>
                {this.props.fetching?<span>FETCHING IN PROGRESS!</span>:
                <ul>
                    {this.props.users.map((user, index) =>
                    <li>
                        <ul key={index}>
                            <li><h4>{user.name}</h4></li>
                            <li><h5>{user.username}</h5></li>
                            <li>{user.phone}</li>
                            <li>{user.website}</li>
                            <li>{user.email}</li>
                        </ul>
                    </li>
                    )}
                </ul>
                }
            </div>
        )
    }
}