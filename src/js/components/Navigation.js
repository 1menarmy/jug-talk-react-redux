/**
 * Created by kanitmar on 05.02.17.
 */
import React from 'react';
import {Link} from 'react-router';

export default class Navigation extends React.Component{
    render(){
        return (
            <nav>
                <ul class="nav nav-tabs">
                    <li >
                        <Link to="/" >Home</Link>
                    </li>
                    <li>
                        <Link to="/testing" >Testing</Link>
                    </li>
                    <li>
                        <Link to="/integration" >Integration</Link>
                    </li>
                </ul>
            </nav>
        );
    }
}

