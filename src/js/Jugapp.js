/**
 * Created by kanitmar on 04.02.17.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import HomePage from './pages/HomePage';
import IntegrationPage from './pages/IntegrationPage';
import TestingPage from './pages/TestingPage';
import Title from './components/Title';
import {ThemeSwitcher, ThemeChooser} from 'react-bootstrap-theme-switcher';
import {Router, Route, hashHistory} from 'react-router';
import Counter from './components/Counter';
import Todos from './components/Todos';
import store from './Store.js';
import {Provider} from 'react-redux';

export default class Jugapp extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <ThemeSwitcher themePath="/themes" defaultTheme="cyborg">
                    <div>
                        <ThemeChooser/>
                        <Title proppedTitleText="Master for all Titles that is actually outside routing"/>
                        <Counter
                            value={store.getState().counterReducer}
                            onIncrement={() => store.dispatch({type: 'INCREMENT'})}
                            onDecrement={() => store.dispatch({type: 'DECREMENT'})}
                        />
                        <Todos
                            todos={store.getState().todosReducer}
                            addTodo={(todo) => store.dispatch({type: 'ADD_TODO', payload: todo})}
                        />
                        <Router history={hashHistory}>
                            <Route path="/" component={HomePage}/>
                            <Route path="/integration" component={IntegrationPage}/>
                            <Route path="/testing" component={TestingPage}/>
                        </Router>
                    </div>
                </ThemeSwitcher>
            </Provider>
        );
    }
}
const render = () => ReactDOM.render(<Jugapp/>, document.getElementById('Jugapp'));
render();
store.subscribe(render);