/**
 * Created by kanitmar on 04.02.17.
 */
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
module.exports ={
    entry: './src/js/Jugapp.js',
    output: {
        filename: 'jugapp.bundled.js',
        path: __dirname + '/build'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                options: {
                    cacheDirectory: true,
                    presets: ['es2015', 'react', 'stage-0'],
                    plugins: ['react-html-attrs', 'transform-decorators-legacy'],
                }
            }
        ]
    },
    plugins: [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin({mangle: false, sourcemap: false}),
        new CopyWebpackPlugin([
                {context: 'node_modules/react-bootstrap-theme-switcher/themes/', from: '**/*', to: 'themes/'}
            ],
            {copyUnmodified: true}
        ),
    ],
    watch:true
}
